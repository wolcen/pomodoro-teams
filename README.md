# Pomodoro Teams

Pomodoro timers for teams. Goals: assist team working cadence, schedule co-working sessions, maintain team status, time tracking/exports.