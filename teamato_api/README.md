# Teamato API

This will be the backend of the Teamato project. This API should record pomodoro's that take place, and features needed to provide team status and coordination.

# Running

Get rust. Haven't determined which build to use yet.

```bash
$ cargo run
```
