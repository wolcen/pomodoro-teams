use actix_web::{web, App, HttpResponse, HttpServer, Responder};
use serde::Deserialize;
use serde::Serialize;

#[derive(Deserialize)]
struct User {
    id: String,
    username: String,
}

#[derive(Serialize, Deserialize)]
struct Pomodoro {
    user_id: String,
    start_time: String,
    end_time: String,
}

async fn start_pomodoro(_user: web::Json<User>) -> impl Responder {
    // TODO: Store the new pomodoro in the database
    HttpResponse::Ok().json("Pomodoro started")
}

async fn end_pomodoro(_pomodoro: web::Json<Pomodoro>) -> impl Responder {
    // TODO: Update the pomodoro's end_time in the database
    HttpResponse::Ok().json("Pomodoro ended")
}

async fn get_user_pomodoros(_user_id: web::Path<String>) -> impl Responder {
    // TODO: Retrieve the pomodoros for the specified user from the database
    let example = Pomodoro {
        user_id: String::from("Jim"),
        start_time: String::from("25 min ago"),
        end_time: String::from("now"),
    };
    HttpResponse::Ok().json(example)
}

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .route("/start", web::post().to(start_pomodoro))
            .route("/end", web::post().to(end_pomodoro))
            .route("/pomodoros/{user_id}", web::get().to(get_user_pomodoros))
    })
    .bind("127.0.0.1:8080")?
    .run()
    .await
}
